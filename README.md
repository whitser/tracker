# Time Tracker #

Very basic project time tracker program

### What's in this repository?###

* Small time tracker that allows users to start and stop a project timer and to resume existing tasks (index.php)
* counter.js to display a counter when a project is in progress to show the user how much time they have spent
* SQL file tracker.sql to allow setup of tables. Works on mysql and MariaDB
* db.php include file to allow database access
* alltasks.php to view all tasks entered on the system

### How do I get set up? ###

* Access database and run the tracker.sql file
* Configure db.php for the local database details
* Ensure the php and js files are in the same web folder
* Access that web folder and the index file should run

### What it does ###

* Allows user to enter a project name and start, displaying any existing projects
* When the project has started, the user is shown the current time elapsed (and total time for existing projects)
* The user has the option to stop the project when they want. This will display a summary of the current time used
* The user can then return to the start to begin another project
* There is an option to display the full list of task actions
