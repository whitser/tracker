<?php
// page to time things
include('db.php');

// If the form has been submitted with a project name, do some checking on the submitted string before saving to the database.
// At the same time check whether the project is new or the resumption of an existing project
if (isset($_POST['submitted'])) {
  if ($_POST['submitted']==1 && strlen($_POST['name'])>0) {
    $start = time();
    $name=$_POST['name'];
    $name=preg_replace('/\s/','/\_/',$name);
    $name=preg_replace('/\W/','',$name);
    $name=strtolower($name);
    // Stop the script if junk was entered into the name field and allow user to return to the form
    if (strlen($name)==0) {
       echo "<div>The project name entered has no alphanumeric characters so cannot be processed</div>";
       echo "<div><a href=\"index.php\">Return to index</a></div>";
       die();
    }
    
    // insert new task into the database
    $dbname=mysqli_real_escape_string($link,$name);
    $query="insert into tracker.instance_log (name,actiontype) values ('$name','start')";
  if (!mysqli_query($link,$query)) {
	   echo "error with query $query";
	}
	$query="select name, sum(duration) totdur from durations group by name having name='$name'";
	$result=mysqli_query($link,$query);
  
  // see if the project is new or resumed, output text based on that. Output the sum of time already used on an existing
  // project in h m s
  
	if (mysqli_num_rows($result)==1) {
	  while ($row = mysqli_fetch_array($result)) {
		$tothours=floor($row['totdur']/3600);
		$totmins=floor(($row['totdur']-$tothours)/60);
		$totsecs=floor($row['totdur'] % 60);
		$top = "<div>Exisiting project ".htmlspecialchars($name)." has been resumed.</div>";
		$existing = "<div>The previous time used on this project was $tothours h $totmins m $totsecs s.</div>";
	  }
	} else {
	  $top = "<div>Project saved with name ".htmlspecialchars($name)."</div>";
	  $existing="<div>This is a new project</div>";
	}
    echo $top;
    echo "<div>Your start time is ".date('H:i:s',$start)." on ".date('D, d-m-Y',$start).".</div>";
    echo "<form name=\"Stop\" method=\"post\"><input type=\"hidden\" name=\"name\" value=\"$name\" />";
    echo "<input type=\"hidden\" name=\"start\" value=\"$start\" />";
    echo "<br /><p>Click here to finish task: <input type=\"submit\" name=\"Stop\" value=\"Stop\" /></p>";
    include('counter.js');
    
    // js counter to show user how much time they have spent on a current active project 
    echo "<div id=\"counter\"></div>";
    // show the user the previous time used so they can see how much time they have spent overall
    echo $existing;
    die();
  } else {
    // Let user know if they didn't enter anything in the name field
    echo "<div>Either you didn't enter your name or something untoward has happened! Please try again...</div>";
  }
}

//control of flow once user stops the current task

if (isset($_POST['start'])) {
    $end = time();
    $start=$_POST['start'];
    $name=$_POST['name'];
// insert individual stop task to the log table    
    $dbname=mysqli_real_escape_string($link,$name);
    $query="insert into tracker.instance_log (name,actiontype) values ('$name','end')";
  	mysqli_query($link,$query);
// calculate time used on current task and add it to the duration table. Show the user the details of the current session	
    $duration = $end - $start;
    //echo "<div>End time is: ".$end."</div>";
    echo "<div>Project name: ".htmlspecialchars($name)."</div>";
    echo "<div>Your start time was ".date('d-m-y H:i:s',$start).".</div>";
    echo "<div>Your endtime was ".date('d-m-y H:i:s',$end).".</div>"; 
    echo "<div>The time taken for the current task was ".$duration." seconds.</div>";
    $query="insert into tracker.durations (name,duration) values ('$name',$duration)";
	mysqli_query($link,$query);
  // allow the user to return to the index
    echo "<br /><p>Click <a href=\"index.php\">here</a> to start a new project and view list of existing projects.</p>";
    die();
}
// HTML form on firstload of page

echo '<html>
<body>
<form name="starttime" method="post" target="index.php">
  <div>Hello, please enter the project name below and click start.</div>
  <div>Note that project names are not case sensitive and should only contain alphanumeric characters, spaces or underscores.</div>
  <div>All spaces will be replaced with underscores to save project names as a single string.</div>
  <div>If you wish to continue an existing project, existing project names are below.</div>
  <div><input type="text" name="name" /></div>
  <div><input type="submit" name="Start" value="Start"/><input type="hidden" name="submitted" value="1" /></div>
</form>';

// if there are existing projects, list them here with times used and also to show the user the correct names of the existing projects.
// in a full system, there would be a dropdown list or radio button list here to allow the user to directly select an existing project
// without having to type in the box

    $query="select name, sum(duration) totdur from durations group by name";
	$result=mysqli_query($link,$query);
	if (mysqli_num_rows($result)>0) { 
	  echo "<table><tr><th>Project Name</th><th>Hours</th><th>Minutes</th><th>Seconds</th></tr>";
	  while ($row = mysqli_fetch_array($result)) {
		echo "<tr><td>".$row['name']."</td>";
		$tothours=floor($row['totdur']/3600);
		$totmins=floor(($row['totdur']-$tothours)/60);
		$totsecs=floor($row['totdur'] % 60);  
		echo "<td>$tothours</td>";
		echo "<td>$totmins</td>";
		echo "<td>$totsecs</td>";
		echo "</tr>";
	  }
// allow the user to view a complete list of all start/stop actions on the system
    echo "<tr><td colspan=\"4\"><a href=\"alltasks.php\">View all individual task entries</a></td></tr>";
	  echo "</table>";
	} else {
	  echo "<div>No projects have been saved on the system yet.</div>";
  }
  echo "</body></html>";