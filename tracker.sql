CREATE DATABASE IF NOT EXISTS tracker;

CREATE TABLE IF NOT EXISTS tracker.instance_log (
  id int NOT NULL auto_increment primary key,
  name varchar(255),
  actiontype char(5),
  occured timestamp
) default character set utf8;


CREATE TABLE IF NOT EXISTS tracker.durations (
  id int NOT NULL auto_increment primary key,
  name varchar(255),
  duration bigint(20)
) default character set utf8;


