<?php
// to show all tasks, listed by most recent project worked on first
include('db.php');
// query the instance_log table which is written to each time a start or end action occurs
$query = "select * from instance_log order by occured desc";
echo "<div>Full list of tasks started and finished on the system</div>";
$result=mysqli_query($link,$query);
	if (mysqli_num_rows($result)>0) { 
	  echo "<table><tr><th>Action</th><th>Project Name</th><th>Time</th></tr>";
	  while ($row = mysqli_fetch_array($result)) {
		echo "<tr><td>".$row['actiontype']."</td>"; 
		echo "<td>".$row['name']."</td>";
		echo "<td>".$row['occured']."</td>";
		echo "</tr>";
	  }
	  echo "</table>";
	} else {
		// and in case anyone reaches this page before any tasks exist
	  echo "<div>No tasks have been saved on the system yet.</div>";
  }  

echo "<div></div>";
echo "<div><a href=\"index.php\">Return to index</a></div>";

?>