<script>
// calculate now minus the start time, updating every second, and output this to the page
var start = <?php echo $start; ?>;
											   
// set interval to 1 second
var sec = setInterval(function() {
																
  // Get current time
  var now = new Date().getTime();
    
  // calculate time elapsed
  var elapsed = now-(start*1000);
    
  // Time calculations for days, hours, minutes and seconds
  // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor(elapsed / (1000 * 60 * 60));
  var minutes = Math.floor((elapsed % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((elapsed % (1000 * 60)) / 1000);
    
  // Output the result in a div with id="counter"
  
  document.getElementById("counter").innerHTML = "This session has lasted " + hours + "h "
  + minutes + "m " + seconds + "s so far.";
  
}, 1000);
</script>